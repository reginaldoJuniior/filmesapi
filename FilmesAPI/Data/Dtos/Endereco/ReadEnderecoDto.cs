﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmesAPI.Data.Dtos
{
    public class ReadEnderecoDto
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "O Campo Logradouro é obrigatório")]
        public string Logradouro { get; set; }
        [Required(ErrorMessage = "O Campo Bairro é obrigatório")]
        public string Bairro { get; set; }
        public int Numero { get; set; }

        public ReadEnderecoDto()
        {
        }
    }
}
