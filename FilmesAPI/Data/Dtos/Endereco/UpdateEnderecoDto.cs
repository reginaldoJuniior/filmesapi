﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmesAPI.Data.Dtos
{
    public class UpdateEnderecoDto
    {

        [Required(ErrorMessage = "O Campo Logradouro é obrigatório")]
        public string Logradouro { get; set; }
        [Required(ErrorMessage = "O Campo Bairro é obrigatório")]
        public string Bairro { get; set; }
        [Required(ErrorMessage = "O Campo Numero é obrigatório")]
        public int Numero { get; set; }

        public UpdateEnderecoDto()
        {
        }
    }
}
