﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FilmesAPI.Data.Dtos
{
    public class UpdateCinemaDto
    {

        [Required(ErrorMessage = "O Campo Nome é obrigatório")]
        public string Nome { get; set; }

        public UpdateCinemaDto()
        {
        }
    }
}
